## Code for the Neo4j GraphAcademy Introduction to LLMs using Ollama

Do you want to connect a graph db to an LLM but don't want to give OpenAI money?

Well this project is for you! I rewrote the code samples from the course to use ollama/llama2 model and embeddings so I didn't have to supply an OpenAI key.

1. Sign up for: https://graphacademy.neo4j.com/courses/llm-fundamentals
2. [Configure ollama](https://gitlab.com/-/snippets/3640592)
3. Configure your own .env file (See: https://sandbox.neo4j.com for your connection details):
```
NEO4J_URL=bolt://IP.IP.IP.IP:7687
NEO4J_USER=neo4j
NEO4J_PASS=yourpass
```
4. During the course DO NOT create the OpenAI embedding index as that will, well, create OpenAI's embedding. We need llama2's embeddings. Note that your 'free' Neo4j database must be 5.18 or above as that is when neo4j began to support 4096 dimensions for embedding indexes.
5. Note you don't have to run creating the movie plot embedding(s) code block, as you can just reference the url directly: `https://gitlab.com/idawson/neo4j-graphacademy-llama2/-/raw/main/movie-plot-embeddings.csv?ref_type=heads&inline=false`


If you bork your neo4j database, you can always destroy it and create a new one from their templates called "Recommendations" and then recreate the indexes.